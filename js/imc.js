function arribaMouse(){

    let parrafo = document.getElementById('parrafo');
    parrafo.style.color = 'yellow';
    parrafo.style.fontSize = '18px';
    parrafo.innerHtml += "Hola mundo";

}

function salir(){

    let parrafo = document.getElementById('parrafo');
    parrafo.style.color = 'blue';
    parrafo.style.fontSize = '15px';

}

const btnCalcular = document.getElementById('btnCalcular');
btnCalcular.addEventListener('click', function calcular()
{

    let peso = document.getElementById('peso').value;
    let altura = document.getElementById('altura').value;

    // Convertir de centímetros a metros
     altura = altura / 100; 

     let imc = peso / (altura * altura);

    document.getElementById('resultado').value = imc.toFixed(2);
});

